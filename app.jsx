import React from 'react';

import { AppLayer } from '../../turtleos-am/am';

import { useSelector, useDispatch } from 'react-redux';

import { WALLPAPERS } from '../../../assets/turtle-wallpaper-collection/wallpapers';

import { Scrollbars } from 'react-custom-scrollbars';

import icon from './icon.png';

function App(props) {
    const wallpaper = useSelector(state=>state.wallpaper);
    const dispatch = useDispatch();
    function refreshView(wp) {
        let tmp = [];
        for (let {id, bg} of Object.values(WALLPAPERS)) {
            tmp.push(
                <div key={id} style={{
                    ...bg,
                    minWidth:'200px',
                    width:`calc( 80% - ${(wp.id===id)?'6px':'0px'})`,
                    transition:'.2s',
                    minHeight:'150px',
                    height:'30%',
                    margin:'5%',
                    border:(wp.id===id)?'3px solid #458588':'3px solid transparent',
                    justifyContent:'stretch',
                    alignItems:'stretch'
                }} onClick={()=>{
                    dispatch({
                        type:'wallpaper:change',
                        payload:{
                            wallpaper:id
                        }
                    });
                }}>
                </div>
            );
        }
        return tmp;
    }
    return (
        <div style={{
            position:'absolute',
            top:0,
            left:0,
            width:'100%',
            height:'100%',
            display:'flex',
            flexDirection:'column',
            justifyContent:'center',
            alignItems:'center'
        }}>
          {/*Header*/}
          <div style={{
              width:'100%',
              height:'3rem',
              background:'#3c3836',
              color: '#ebdbb2',
              display:'flex',
              justifyContent:'center',
              alignItems:'center',
              position:'absolute',
              top:0,
              left:0,
              zIndex:2,
              boxShadow:'0px 0px 10px -5px black'
          }}>
            CARLON
          </div>
        <div style={{height:'3rem'}}>
            
          </div>
          {/*ScrollView*/}
          <Scrollbars style={{
              margin:'0 5%',
              width:'90%',
              overflow:'hidden',
              display:'flex',
              flexDirection:'column',
              justifyContent:'center',
              alignItems:'center'
        }}>
            {refreshView(wallpaper)}
        </Scrollbars>
        </div>
    );
}
const LAYER = new AppLayer('Carlon', 'icu.ccw.turtleos.carlon',{
    render: (intent={})=>{return (<App data={intent}/>);},
    icon: icon,
    dimens: {
        min: [250, 400],
        default:[300,500],
        max: [500, 600]
    }
});

export {
    LAYER
}
